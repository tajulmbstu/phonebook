
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// import data from '../data';
import { listContacts, deleteContact, saveContact } from '../actions/contactActions';

function PhonebookScreen(props) {

    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [modalVisible, setModalVisible] = useState(false);

    const openModal = (contact) => {
        setModalVisible(true);
        setName(contact.name);
        setPhoneNumber(contact.phoneNumber);

        setId(contact._id);
    }

    const contactList = useSelector(state => state.contactList); // eslint-disable-next-line
    const { loading, contacts, error } = contactList;

    const contactSave = useSelector(state => state.contactSave);
    const { loading: loadingSave, success: successSave, error: errorSave } = contactSave;

    const contactDelete = useSelector(state => state.contactDelete); // eslint-disable-next-line
    const { loading: loadingDelete, success: successDelete, error: errorDelete } = contactDelete;

    const dispatch = useDispatch();

    const submitHandler = (e) => {
        e.preventDefault();
        if (name && phoneNumber) {
            isValidPhone(phoneNumber) ? dispatch(saveContact({
                _id: id,
                name, phoneNumber
            })) : alert("Please enter a valid phone number");
        }
    }

    const deleteHandler = (contact) => {
        dispatch(deleteContact(contact._id));
    }
    const isValidPhone = (phoneNumber) => {
        var found = phoneNumber.search(/^(?:\+?88)?01[135-9]\d{8}$/);
        if (found > -1) {
            return true;
        }
        else {
            return false;
        }
    }
    useEffect(() => {
        document.title = "Phonebook";
        if (successSave) {
            setModalVisible(false);
        }
        dispatch(listContacts());
        return () => {
            //
        };
    }, [successSave, successDelete, dispatch]);

    return (
        loading ? <div>Loading...</div> :
            error ? <div className="error-message"> {error} </div> :
                <div className="contacts">
                    {modalVisible &&
                        <div className="contact">
                            <div className="contact-info">
                                <div className="form">
                                    <form onSubmit={submitHandler}>
                                        <ul className="form-container">
                                            <li>
                                                <h3>{id ? "Update Contact" : "Create New Contact"} </h3>
                                            </li>
                                            <li>
                                                {loadingSave && <div> Loading...</div>}
                                                {errorSave && <div className="error-message">{errorSave}</div>}
                                            </li>
                                            <li>
                                                <label htmlFor="name" > Name </label>
                                                <input type="text" name="name" id="name" value={name || ""} onChange={(e) => { setName(e.target.value) }} placeholder="Enter name" required></input>
                                            </li>
                                            <li>
                                                <label htmlFor="phoneNumber"> Phone Number </label>
                                                <input type="tel" name="phoneNumber" value={phoneNumber || ""} id="phoneNumber" onChange={(e) => setPhoneNumber(e.target.value)} required />
                                                <label htmlFor="phoneNumber" className="error-message" > Note: Only Bangladeshi phone number allowed  </label>

                                            </li>
                                            <li>
                                                <button type="submit" className="button primary">{id ? "Update" : "Create"}</button>
                                            </li>
                                            <li>
                                                <button type="button" onClick={() => setModalVisible(false)} className="button secondary">Back</button>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </div>
                    }
                    {!modalVisible &&
                        <>
                            <div className="contact-header">
                                <h3> Contacts </h3>
                                <button className="button primary" onClick={() => openModal({})}>Create Contact</button>
                            </div>
                            <div className="contact-orders content-margined">
                                {
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {contacts.map(contact => (<tr key={contact._id}>
                                                <td>{contact.name}</td>
                                                <td>
                                                    <ul className="contactlist-action">
                                                        <li><button type="button" onClick={() => openModal(contact)} className="button primary"> Details </button></li>
                                                        <li><button type="button" onClick={() => deleteHandler(contact)} className="button primary">Delete</button></li>
                                                    </ul>

                                                </td>
                                            </tr>))}
                                        </tbody>
                                    </table>
                                }
                            </div>
                        </>
                    }


                </div>);
}

export default PhonebookScreen;